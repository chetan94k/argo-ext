((window) => {
    const component = () => {
        return React.createElement("iframe",
            { src: "http://172-105-44-64.ip.linodeusercontent.com:8080/static/jira/",
                width: "100%",
                height: "375"},
        );
    };
    window.extensionsAPI.registerResourceExtension(
        component,
        "*",
        "*",
        "Jira Status"
    );
})(window);